import React, { useState } from "react";
import {
  Prompt
} from "react-router-dom";
const Home = () => {
  let [isBlocking, setIsBlocking] = useState(false);

  return (<React.Fragment>
    <h2>Home</h2>
    <form
      onSubmit={event => {
        event.preventDefault();
        event.target.reset();
        setIsBlocking(false);
      }}
    >
      <Prompt
        when={isBlocking}
        message={location =>
          `Are you sure you want to go to ${location.pathname}`
        }
      />
      <p>
        <input
          size="50"
          placeholder="type something to block transitions"
          onChange={event => {
            setIsBlocking(event.target.value.length > 0);
          }}
        />
      </p>
    </form>
  </React.Fragment >);
}

export default Home