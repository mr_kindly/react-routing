import React from "react";
import UsersList from "../components/users/UsersList";
import {
  useLocation
} from "react-router-dom"

const useQuery = () => new URLSearchParams(useLocation().search);

const users = [{ name: 'John Smith', id: 1, gender: 'male', age: 35 }, { name: 'Tom Sawyer', id: 2, gender: 'male', age: 24 }, { name: 'Anna Smith', id: 3, gender: 'female', age: 18 }];

const Users = () => {
  const query = useQuery();
  const gender = query.get('gender');
  const age = query.get('age');
  let filteredUsers = gender ? users.filter(u => u.gender === gender) : users;
  filteredUsers = age ? filteredUsers.filter(u => u.age >= age) : filteredUsers;

  return <React.Fragment>
    <h2>Users</h2>
    <UsersList users={filteredUsers}></UsersList>
  </React.Fragment>;
}

export default Users;