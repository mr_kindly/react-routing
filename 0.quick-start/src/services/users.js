
const users = [
  { name: 'John Smith', id: 1, gender: 'male', age: 35 },
  { name: 'Tom Sawyer', id: 2, gender: 'male', age: 24 },
  { name: 'Anna Smith', id: 3, gender: 'female', age: 18 }
];

export const getUsers = () => users;

export const getUserById = (id) => users.find(u => u.id === id);