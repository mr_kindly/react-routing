import React from "react";
import { Link, Route, Switch, useParams, useRouteMatch } from "react-router-dom";

const Topic = () => {
  let { topicId } = useParams();
  return (
    <h3>
      {topicId}
    </h3>
  )
}

const About = () => {
  let { path, url } = useRouteMatch();

  return (
    <React.Fragment>
      <h2>About</h2>
      <ul className="list-group">
        <li className="list-group-item">
          <Link to={`${url}/react`}>React</Link>
        </li>
        <li className="list-group-item">
          <Link to={`${url}/react-router`}>React Router</Link>
        </li>
      </ul>
      <Switch>
        <Route exact path={path}>
          <p>Please select a topic</p>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </React.Fragment>
  );
}

export default About;