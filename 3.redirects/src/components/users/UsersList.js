import React from "react";

export default function UsersList() {
  let users = [{ name: 'John Smith', id: 1 }, { name: 'Tom Sawyer', id: 2 }];
  const userList = users.map((user) => <li key={user.id}>{user.name}</li>);
  return <ul>
    {userList}
  </ul>;
}