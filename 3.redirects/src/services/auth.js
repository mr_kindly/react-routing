export const auth = {
  isAuthenticated: false,
  signIn(cb) {
    this.isAuthenticated = true;
    if (cb) {
      cb();
    }

  },
  sigOut(cb) {
    this.isAuthenticated = false;
    if (cb) {
      cb();
    }
  }
}