import React from "react";
import { auth } from "./../services/auth";
import {
  useHistory,
  useLocation
} from "react-router-dom"


const Login = () => {
  let history = useHistory();
  let location = useLocation();
  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => auth.signIn(() => {
    history.replace(from);
  })


  return (<React.Fragment>
    <p>Welcome</p>
    {auth.isAuthenticated ? <button onClick={() => auth.signOut()}>Sign Out</button> : <button onClick={login}> Sign In</button>}
  </React.Fragment>)
}

export default Login;