import React from "react";
import { Link } from 'react-router-dom';

export default function UsersList({ users }) {

  const userList = users.map((user) => <li key={user.id} className="list-group-item">
    <Link to={`/user/${user.id}`}>
      {user.name}
    </Link>
  </li>);
  return <ul className="list-group">
    {userList}
  </ul>;
}