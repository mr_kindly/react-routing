import React from "react";
import { getUsers } from "../services/users";
import UsersList from "./../components/users/UsersList";



const Users = () => {
  const users = getUsers();
  return <React.Fragment>
    <h2>Users</h2>
    <UsersList users={users}></UsersList>
  </React.Fragment>;
}

export default Users;