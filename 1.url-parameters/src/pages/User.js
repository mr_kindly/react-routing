import React from "react";
import {
  useParams
} from "react-router-dom";
import { getUserById } from "../services/users";

const User = () => {
  let { id } = useParams();
  const user = getUserById(parseInt(id));
  return (<React.Fragment>
    <h2>User {id}</h2>
    <p>{user.name}</p>
    <p>{user.age}</p>

  </React.Fragment>);
}

export default User;