import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import About from './pages/About';
import Users from './pages/Users';
import User from './pages/User';
import Home from './pages/Home';


export default function App() {
  return (
    <Router>

      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-2">
            <nav>
              <ul className="nav nav-pills flex-column">
                <li className="nav-item">
                  <NavLink to="/" className="nav-link" exact="true">Home</NavLink >
                </li>
                <li className="nav-item">
                  <NavLink to="/users" className="nav-link">Users</NavLink >
                </li>
                <li className="nav-item">
                  <NavLink to="/about" className="nav-link">About</NavLink >
                </li>
              </ul>
            </nav>
          </div>
          <div className="col-sm-8">
            {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/about" component={About} />
              <Route path="/users" component={Users} />
              <Route path="/user/:id" component={User} />
              <Route path="/" component={Home} />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

