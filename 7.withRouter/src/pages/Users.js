import React from "react";
import UsersList from "../components/users/UsersList";
import {
  useLocation, withRouter
} from "react-router-dom"

const users = [{ name: 'John Smith', id: 1, gender: 'male', age: 35 }, { name: 'Tom Sawyer', id: 2, gender: 'male', age: 24 }, { name: 'Anna Smith', id: 3, gender: 'female', age: 18 }];


class Users extends React.Component {
  render() {
    const {match, location, history} = this.props;
    console.info('MATCH: ', match);
    console.info('HISTORY: ', history);
    const query = new URLSearchParams(location.search);
    const gender = query.get('gender');
    const age = query.get('age');
    let filteredUsers = gender ? users.filter(u => u.gender === gender) : users;
    filteredUsers = age ? filteredUsers.filter(u => u.age >= age) : filteredUsers;
    return <React.Fragment>
      <h2>Users</h2>
      <UsersList users={filteredUsers}></UsersList>
    </React.Fragment>;
  }
};

export default withRouter(Users);