import React from "react";

export default function UsersList({users}) {
  const userList = users.map((user) => <li key={user.id}>{user.name}</li>);
  return <ul>
    {userList}
  </ul>;
}